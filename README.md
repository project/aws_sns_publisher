**AWS SNS Publisher Module**
------------------

CONTENTS OF THIS FILE
---------------------

* Installation
* Requirements
* Test Connection
* Details


INTRODUCTION
------------
Use this module to Publish to AWS SNS Topic on
entity create, update and delete.

REQUIREMENTS
------------
Require aws-php-sdk library (install this module using composer to get this
library).


INSTALLATION
------------
* Install as you would normally install a contributed Drupal module.
Preferrably using Composer.


DETAILS
-------------
* For setting configurations,
* visit
* ``Configuration > AWS SNS Publisher`` (``admin/config/aws-sns-publisher``)
* Set AWS credentials on AWS Configuration page
* (``admin/config/aws_sns_publisher/awsconfig``)
* Set AWS Mapping to map content types/ entity types with AWS SNS Topic by
* visiting AWS SNS Mapping
* page (``admin/config/aws_sns_publisher/awssnsmapping``)
* For testing connection if credentials added on AWS Configuration page is
* correct.
* Visit ``admin/config/aws-sns/test``
* Functionality : When any content is updated, a notification is sent to
* AWS SNS with entity passed as payload in json
* format.
* Notifications pushed to AWS SNS can be read by setting up an AWS SQS or other
* subscription service for that AWS SNS Topic.
