<?php

namespace Drupal\aws_sns_publisher;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Aws\Sns\SnsClient;

/**
 * Class AspServices to define Utility functions.
 *
 * @package Drupal\aws_sns_publisher\Services
 */
class AspServices {

  /**
   * The Logger Interface.
   *
   * @var Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The ConfigFactory for getting SNS configurations.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new MostPopularArticle object.
   *
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager.
   * @param Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The Logger Interface.
   * @param Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The Config Factory for accessing SNS configurations.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactoryInterface $logger, ConfigFactoryInterface $configFactory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->configFactory = $configFactory;
  }

  /**
   * Convert a multi-dimensional array into a single-dimensional array.
   *
   * @param array $array
   *   The multi-dimensional array.
   *
   * @return array
   *   Array list.
   */
  public function flattenArray(array $array) {
    if (!is_array($array)) {
      return FALSE;
    }
    $result = [];
    foreach ($array as $key => $value) {
      $result[$value['TopicArn']] = $value['TopicArn'];
    }
    return $result;
  }

  /**
   * Fetch topic list from aws sns.
   *
   * @return array
   *   Array list of topics.
   */
  public function listTopic() {
    $config = $this->configFactory->get('aws_sns_publisher.awsconfig');

    $params = [
      'credentials' => [
        'key' => $config->get('aws_key') ? $config->get('aws_key') : '',
        'secret' => $config->get('aws_secret') ? $config->get('aws_secret') : '',
      ],
      'region' => 'us-east-2',
      'version' => 'latest',
    ];
    $topics = [];
    if (!empty($config->get('aws_key')) && !empty($config->get('aws_secret'))) {
      $client = new SnsClient($params);
      try {
        $result = $client->listTopics([]);
        $topics = $this->flattenArray($result->search('Topics'));
      }
      catch (AwsException $e) {

        // Output error message if fails.
        var_dump($e->getMessage());
      }
    }
    return $topics;
  }

  /**
   * Fetch entity list from aws sns.
   *
   * @return array
   *   Array list of an entities.
   */
  public function listEntities() {
    $entity_types = $this->entityTypeManager->getDefinitions();
    $moderation_info = \Drupal::service('content_moderation.moderation_information');
    $selected_bundles = [];
    foreach ($entity_types as $entity_type) {
      if (!$moderation_info->canModerateEntitiesOfEntityType($entity_type)) {
        continue;
      }
      else {
        $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type->id());
        $selected_bundles[$entity_type->id()] = array_keys($bundles);
      }

    }
    return $selected_bundles;
  }

  /**
   * Function to push to AWS SNS.
   */
  public function publishToAwsSns($subject, $message, $topic = '') {
    $config = $this->configFactory->get('aws_sns_publisher.awsconfig');

    $params = [
      'credentials' => [
        'key' => $config->get('aws_key'),
        'secret' => $config->get('aws_secret'),
      ],
      'region' => 'us-east-2',
      'version' => 'latest',
    ];
    $client = new SnsClient($params);

    $result = $client->publish([
      'TopicArn' => ($topic !== '') ? $topic : $config->get('aws_sns_default_topic_arn'),
      // Message is required.
      'Message' => $message,
      'Subject' => $subject,
      'MessageStructure' => 'aws sns notify',
    ]);

    $message_id = $result->search('MessageId');
    \Drupal::messenger()->addStatus('AWS SNS message published. Msg id: ' . $message_id);
    return $message_id;
  }

  /**
   * Get the flat entity object.
   *
   * @param object $entity
   *   Entity Object.
   *
   * @return array
   *   Array of an entity.
   */
  public function normalize($entity) {

    // Covert an entity object to an array.
    $entityArray = $entity->toArray();
    foreach ($entityArray as $name => $value) {
      if (isset($value[0])) {
        if (array_key_exists('target_id', $value[0])) {
          // Exclude some of the referenced entities.
          if (!in_array($name, ['user_id', 'revision_user', 'vid'])) {
            // Get all referenced entities.
            $childEntities = $entity->{$name}->referencedEntities();
            foreach ($childEntities as $childEntity) {
              if (!empty($childEntity)) {
                $attributes[$name][] = $this->normalize($childEntity);
              }
            }
          }
        }
        else {
          if (isset($value[0]['value'])) {
            $attributes[$name] = $value[0]['value'];
          }
        }
      }
      else {
        $attributes[$name] = NULL;
      }
    }
    return $attributes;
  }

}
