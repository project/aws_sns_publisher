<?php

namespace Drupal\aws_sns_publisher\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\aws_sns_publisher\AspServices;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class AwsSnsMapping.
 */
class AwsSnsMapping extends ConfigFormBase {

  /**
   * AWS SNS Publisher ASP Services.
   *
   * @var \Drupal\aws_sns_publisher\AspServices
   */
  private $aspServices;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AspServices $aspServices) {
    parent::__construct($config_factory);
    $this->aspServices = $aspServices;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('aws_sns_publisher.asp_services')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'aws_sns_publisher.mapping',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aws_sns_mapping';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('aws_sns_publisher.mapping');
    $available_content_entities = $this->aspServices->listEntities();
    $topics = $this->aspServices->listTopic();

    if (empty($topics) || empty($available_content_entities)) {
      $form['help'] = [
        '#type' => 'item',
        '#markup' => t('AWS configuration is missing.'),
      ];
      return $form;
    }

    $form['Title'] = [
      '#type' => 'label',
      '#attributes' => [
        'class' => [
          'heading',
        ],
      ],
      '#title' => 'Publish to SNS :',
      '#title_display' => 'above',
    ];
    foreach ($available_content_entities as $entity_type => $content_entity) {
      $entity_title = ucwords(str_replace('_', ' ', $entity_type));
      $form[$entity_type . '_title'] = [
        '#type' => 'label',
        '#attributes' => [
          'class' => [
            'sub-heading',
          ],
        ],
        '#title' => $entity_title,
        '#title_display' => 'above',
      ];
      foreach ($content_entity as $bundle_name) {
        $bundle_title = ucwords(str_replace('_', ' ', $bundle_name));
        $form[$bundle_name] = [
          '#type' => 'checkbox',
          '#title' => $this->t($bundle_title),
          '#default_value' => $config->get($bundle_name),
        ];
        $form[$bundle_name . 'sns_settings'] = [
          '#type' => 'fieldgroup',
          '#attributes' => [
            'class' => [
              'sns_settings',
            ],
          ],
          '#states' => [
            'invisible' => [
              ':input[name=' . $bundle_name . ']' => [
                'checked' => FALSE,
              ],
            ],
          ],
        ];

        $form[$bundle_name . 'sns_settings'][$bundle_name . '_topic'] = [
          '#type' => 'select',
          '#title' => $this->t('Choose AWS SNS Topic'),
          '#description' => $this->t('Choose SNS topics for @bundle_title to publish', ['@bundle_title' => $bundle_title]),
          '#options' => $topics,
          '#size' => 1,
          '#default_value' => $config->get($bundle_name . '_topic'),
          '#states' => [
            'visible' => [
              ':input[name=' . $bundle_name . ']' => ['checked' => TRUE],
            ],
          ],
        ];

        $form[$bundle_name . 'sns_settings'][$bundle_name . '_flatten'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Flatten Entity'),
          '#default_value' => $config->get($bundle_name . '_flatten'),
          '#description' => $this->t('Enable to push the flat entity object.'),
          '#states' => [
            'visible' => [
              ':input[name= ' . $bundle_name . ']' => ['checked' => TRUE],
            ],
          ],
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $available_content_entities = $this->aspServices->listEntities();
    foreach ($available_content_entities as $entity_type => $content_entity) {
      foreach ($content_entity as $bundle_name) {
        $this->config('aws_sns_publisher.mapping')
          ->set($bundle_name, $form_state->getValue($bundle_name))
          ->set($bundle_name . '_topic', $form_state->getValue($bundle_name . '_topic'))
          ->set($bundle_name . '_flatten', $form_state->getValue($bundle_name . '_flatten'))
          ->save();
      }
    }
  }

}
