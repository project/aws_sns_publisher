# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [8.x-1.0] - 2020-11-20

First stable release of the AWS SNS Publisher module. Enjoy!

### Added

* Initial support for publishing entities data to AWS SNS Topic.
