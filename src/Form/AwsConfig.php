<?php

namespace Drupal\aws_sns_publisher\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AwsConfig.
 */
class AwsConfig extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'aws_sns_publisher.awsconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aws_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('aws_sns_publisher.awsconfig');
    $form['aws_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AWS Key'),
      '#description' => $this->t('AWS key for your account.'),
      '#required' => TRUE,
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('aws_key'),
    ];
    $form['aws_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AWS Secret'),
      '#description' => $this->t('AWS Secret for your account.'),
      '#required' => TRUE,
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('aws_secret'),
    ];
    $form['aws_sns_default_topic_arn'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AWS SNS Default Topic Arn'),
      '#description' => $this->t('Default AWS SNS topic arn. Create topic by visiting AWS account (https://us-east-2.console.aws.amazon.com/sns/v3/home?region=us-east-2#/create-topic)'),
      '#required' => TRUE,
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('aws_sns_default_topic_arn'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('aws_sns_publisher.awsconfig')
      ->set('aws_key', $form_state->getValue('aws_key'))
      ->set('aws_secret', $form_state->getValue('aws_secret'))
      ->set('aws_sns_default_topic_arn', $form_state->getValue('aws_sns_default_topic_arn'))
      ->save();
  }

}
